package be.c4j;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class StreamExample {

    public static void main(String[] args) {
        asList("Andy", "Berten", "Android", "Andrew", "Eddy", "franky")
                .stream()
                .map(Person::new)
                .filter(p -> p.getName().startsWith("A"))
                .sorted((p,p2) -> p.getName().compareTo(p2.getName()))
                .forEach(p -> System.out.println(p.getName()));


        for (String s : asList("Andy", "Berten", "Android", "Andrew", "Eddy", "franky")) {
            Person p = new Person(s);
            if(p.getName().startsWith("A"))
                System.out.println(p.getName());
        }


        Stream<Person> stream2 = Stream.of("Andy", "Borax", "Asterix", "Andrew", "Eddy", "xerxex").map(Person::new);

        List<Student> students = stream2
                .filter(p -> p.getName().toLowerCase().endsWith("x"))
                .map(person -> new Student(person))
                .collect(Collectors.toList());

        students.stream().forEach(p -> System.out.println(p.getPerson().getName()));


    }

    private static class Student {
        private Person person;

        private Student(Person person) {
            this.person = person;
        }

        public Person getPerson() {
            return person;
        }
    }

    private static class Person {
        private String name;

        private Person(String name) {
            this.name = name;

        }


        public String getName() {
            return name;
        }
    }
}
