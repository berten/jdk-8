package be.c4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class MethodReferenceExample {


    public static void main(String[] args) {


        methodReference();
    }


    private static void methodReference() {
        Person p1 = new Person("abc");
        Person p2 = new Person("zbc");

        Person[] people = new Person[]{p1, p2};
        Comparator.comparing(new Function<Person, String>() {
            @Override
            public String apply(Person o) {
                return o.getName();
            }
        });

        Comparator<Person> byNameLambda = Comparator.comparing(p -> p.getName());
        Arrays.sort(new Person[]{p1, p2}, byNameLambda);
        System.out.println(people[0]);
        System.out.println(people[1]);


        people = new Person[]{p1, p2};
        Comparator<Person> byNameMethodReference = Comparator.comparing(Person::getName);
        Arrays.sort(new Person[]{p1, p2}, byNameMethodReference);
        System.out.println(people[0]);
        System.out.println(people[1]);

    }

    private static class Person {


        private String name;

        public Person(String name) {

            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
