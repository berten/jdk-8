package be.c4j;

public interface PrettyTextGenerator {

    public String generatePrettyString();
}
