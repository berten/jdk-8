package be.c4j;

public interface Calculation {
    public int calculate (int first, int second);
}
