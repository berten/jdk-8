package c4j.person;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static c4j.person.Person.Gender.F;
import static c4j.person.Person.Gender.M;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PersonFilterTest {

    private PersonFilter personFilter;

    @Before
    public void setUp() throws Exception {
        personFilter = new PersonFilter();
    }

    @Test
    public void filtersCorrectly() throws Exception {
        Person m17 = new Person(createDate(17), M);
        Person f34 = new Person(createDate((34)), F);
        Person m24 = new Person(createDate(25), M);
        List<Person> persons = personFilter.filterMalePersonsBetween20And25(Arrays.asList(m17, f34, m24), personToTest -> personToTest.getGender() == M && personToTest.getAge() >= 20 && personToTest.getAge() <= 25);

        /*
         List<Person> persons = personFilter.filterMalePersonsBetween20And25(Arrays.asList(m17, f34, m24), new PersonTester<Person>() {

            @Override
            public boolean test(Person personToTest) {
                return personToTest.getGender() == M && personToTest.getAge() >= 20 && personToTest.getAge() <= 25;
            }
        });
         */


        assertEquals(1, persons.size());
        assertTrue(persons.contains(m24));

    }

    @Test
    public void filtersCorrectly_26IsToOld() throws Exception {
        Person m17 = new Person(createDate(17), M);
        Person f34 = new Person(createDate((34)), F);
        Person m24 = new Person(createDate(26), M);
        List<Person> persons = personFilter.filterMalePersonsBetween20And25(Arrays.asList(m17, f34, m24), personToTest -> personToTest.getGender() == M && personToTest.getAge() >= 20 && personToTest.getAge() <= 25);

        /*
         List<Person> persons = personFilter.filterMalePersonsBetween20And25(Arrays.asList(m17, f34, m24), new PersonTester<Person>() {

            @Override
            public boolean test(Person personToTest) {
                return personToTest.getGender() == M && personToTest.getAge() >= 20 && personToTest.getAge() <= 25;
            }
        });
         */

        assertEquals(0, persons.size());

    }

    @Test
    public void filtersCorrectly_20IsOldEnough() throws Exception {
        Person m17 = new Person(createDate(17), M);
        Person f34 = new Person(createDate((34)), F);
        Person m24 = new Person(createDate(20), M);
        List<Person> persons = personFilter.filterMalePersonsBetween20And25(Arrays.asList(m17, f34, m24), personToTest -> personToTest.getGender() == M && personToTest.getAge() >= 20 && personToTest.getAge() <= 25);

        /*
         List<Person> persons = personFilter.filterMalePersonsBetween20And25(Arrays.asList(m17, f34, m24), new PersonTester<Person>() {

            @Override
            public boolean test(Person personToTest) {
                return personToTest.getGender() == M && personToTest.getAge() >= 20 && personToTest.getAge() <= 25;
            }
        });
         */

        assertEquals(1, persons.size());
        assertTrue(persons.contains(m24));

    }

    @Test
    public void filtersCorrectly_19IsToYoung() throws Exception {
        Person m17 = new Person(createDate(17), M);
        Person f34 = new Person(createDate((34)), F);
        Person m24 = new Person(createDate(19), M);
        List<Person> persons = personFilter.filterMalePersonsBetween20And25(Arrays.asList(m17, f34, m24), personToTest -> personToTest.getGender() == M && personToTest.getAge() >= 20 && personToTest.getAge() <= 25);

        /*
         List<Person> persons = personFilter.filterMalePersonsBetween20And25(Arrays.asList(m17, f34, m24), new PersonTester<Person>() {

            @Override
            public boolean test(Person personToTest) {
                return personToTest.getGender() == M && personToTest.getAge() >= 20 && personToTest.getAge() <= 25;
            }
        });
         */

        assertEquals(0, persons.size());
    }

    public Date createDate(int age) {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -age);
        return cal.getTime();
    }
}