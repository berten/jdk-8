package c4j.date;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class DateCalculatorTest {

    private DateCalculator dateCalculator;

    @Before
    public void setUp() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(1984, 7, 19);
        dateCalculator = new DateCalculator(cal.getTime());

    }


    @Test
    public void currentDatePlusOneWeek() {
        assertEquals("26-08-1984", dateCalculator.currentDatePlusOneWeek());
    }

    @Test
    public void nextSunday() {
        assertEquals("26-08-1984", dateCalculator.nextSunday());
    }

    @Test
    public void age() {
        assertEquals(29, dateCalculator.calculateAge());
    }
}