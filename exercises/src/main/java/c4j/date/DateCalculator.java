package c4j.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

public class DateCalculator {


    //private final DateFormat format;
    private final Date subject;


    public DateCalculator(Date subject) {
        //format = new SimpleDateFormat("dd-MM-yyyy");
        this.subject = subject;
    }


    public String currentDatePlusOneWeek() {

        LocalDateTime b = LocalDateTime.ofInstant(subject.toInstant(), ZoneId.of(ZoneId.SHORT_IDS.get("PST")));
        return b.plusDays(7).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
//
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(subject);
//        cal.add(Calendar.DATE, 7);
//        return format.format(cal.getTime());
    }

    public String nextSunday() {


        LocalDateTime b = LocalDateTime.ofInstant(subject.toInstant(), ZoneId.of(ZoneId.SHORT_IDS.get("PST")));
        return b.with(TemporalAdjusters.next(DayOfWeek.SUNDAY)).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));


//        Calendar cal = Calendar.getInstance();
//        cal.setTime(subject);
//        cal.add(Calendar.DATE, Math.abs(cal.get(Calendar.DAY_OF_WEEK) - 8));
//
//        return format.format(cal.getTime());
    }

    public int calculateAge() {

        LocalDateTime b = LocalDateTime.ofInstant(subject.toInstant(), ZoneId.of(ZoneId.SHORT_IDS.get("PST")));

        Period period = b.toLocalDate().until(LocalDate.now());
        return period.getYears();


//        Calendar today = Calendar.getInstance();
//        Calendar birthDate = Calendar.getInstance();
//
//
//        birthDate.setTime(this.subject);
//        if (birthDate.after(today)) {
//            throw new IllegalArgumentException("Can't be born in the future");
//        }
//
//        int age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);
//
//        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
//        if ((birthDate.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_YEAR) > 3) ||
//                (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH))) {
//            age--;
//
//            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
//        } else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH)) &&
//                (birthDate.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH))) {
//            age--;
//        }
//
//        return age;
    }
}
