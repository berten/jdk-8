package c4j.person;

import c4j.date.DateCalculator;

import java.util.Date;

public class Person {
    private final Date birthDate;

    public Gender getGender() {
        return gender;
    }

    public Integer getAge() {
        return new DateCalculator(birthDate).calculateAge();
    }

    public enum Gender {M, F}

    public Gender gender;

    public Person(Date birthDate, Gender gender) {
        this.birthDate = birthDate;
        this.gender = gender;
    }
}
