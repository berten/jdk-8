package c4j.person;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PersonFilter {


    public List<Person> filterMalePersonsBetween20And25(List<Person> persons, PersonTester<Person> predicate) {
        List<Person> filteredPersons = new ArrayList<>();
        for (Person person : persons) {
            if (predicate.test(person)) {
                filteredPersons.add(person);
            }
        }

        return filteredPersons;
    }
}
