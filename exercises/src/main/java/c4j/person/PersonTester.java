package c4j.person;

public interface PersonTester<T> {
    boolean test(Person person);
}
