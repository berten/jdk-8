package be.c4j;

public interface MyInterface {

    public default boolean isEmpty(String testString) {
        return !hasText(testString);
    }

    public static Boolean isNull(String testString) {
        return testString == null;
    }

    public static Boolean hasText(String testString) {
        return !isNull(testString) && testString.length() > 0;
    }
}
