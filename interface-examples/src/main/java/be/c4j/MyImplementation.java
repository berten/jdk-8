package be.c4j;

public class MyImplementation implements MyInterface{
    //@Override
    public Boolean hasText(String testString) { // Does not override interface
        return !MyInterface.isNull(testString) && testString.length() > 3;
    }

    public static void main(String[] args) {
        MyImplementation impl = new MyImplementation();
        if(!impl.isEmpty("abc")) {
            System.out.println("Not Empty");
        }

        if(MyInterface.isNull(null))
            System.out.println("null is null");

        if(!MyInterface.hasText("bac"))
            System.out.println("bac is not long enough");


    }
}
